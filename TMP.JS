import {
    GET_ALL_ATTRIBUTE_SUCCESS,
  
    GET_AN_ATTRIBUTE_SUCCESS,
  
    CREATE_ATTRIBUTE_SUCCESS,

  } from "../config/ActionTypes";
  
  export function getAllAttributeSuccess(data) {
    return { type: GET_ALL_ATTRIBUTE_SUCCESS, data: data };
  }

  
  export function getAnAttributeSuccess(data) {
    return { type: GET_AN_ATTRIBUTE_SUCCESS, data: data };
  }

  
  export function createAttributeSuccess(data){
    return { type: CREATE_ATTRIBUTE_SUCCESS, data: data };
  }
  
