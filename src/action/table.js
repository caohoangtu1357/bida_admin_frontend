import {
  GET_ALL_TABLE_SUCCESS,
  GET_ALL_TABLE_FAILURE,

  GET_AN_TABLE_SUCCESS,
  GET_AN_TABLE_FAILURE,

  CREATE_TABLE_SUCCESS,
  CREATE_TABLE_FAILURE
} from "../config/ActionTypes";

export function getAllTableSuccess(data) {
  return { type: GET_ALL_TABLE_SUCCESS, data: data };
}
export function getAllTableFailure(error) {
  return { type: GET_ALL_TABLE_FAILURE, error: error };
}

export function getAnTableSuccess(data) {
  return { type: GET_AN_TABLE_SUCCESS, data: data };
}
export function getAnTableFailure(error) {
  return { type: GET_AN_TABLE_FAILURE, error: error };
}

export function createTableSuccess(data){
  return { type: CREATE_TABLE_SUCCESS, data: data };
}

export function createTableFailure(error){
  return { type: CREATE_TABLE_FAILURE, error: error };
}