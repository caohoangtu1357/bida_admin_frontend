import {
    GET_ALL_PRODUCT_TYPE_SUCCESS,
  
    GET_AN_PRODUCT_TYPE_SUCCESS,
  
    CREATE_PRODUCT_TYPE_SUCCESS,

  } from "../config/ActionTypes";
  
  export function getAllProductTypeSuccess(data) {
    return { type: GET_ALL_PRODUCT_TYPE_SUCCESS, data: data };
  }

  
  
  export function createProductTypeSuccess(data){
    return { type: CREATE_PRODUCT_TYPE_SUCCESS, data: data };
  }
  
