import {
  GET_ALL_USER_SUCCESS,
  GET_ALL_USER_FAILURE,
  GET_AN_USER_SUCCESS,
  GET_AN_USER_FAILURE,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
} from "../config/ActionTypes";

export function getAllUserSuccess(data) {
  return { type: GET_ALL_USER_SUCCESS, data: data };
}
export function getAllUserFailure(error) {
  return { type: GET_ALL_USER_FAILURE, error: error };
}

export function getAnUserSuccess(data) {
  return { type: GET_AN_USER_SUCCESS, data: data };
}
export function getAnUserFailure(error) {
  return { type: GET_AN_USER_FAILURE, error: error };
}

export function deleteUserSuccess(data) {
  return { type: DELETE_USER_SUCCESS, data: data };
}
export function deleteUserFailure(error) {
  return { type: DELETE_USER_FAILURE, error: error };
}
