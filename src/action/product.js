import {
    GET_ALL_PRODUCT_SUCCESS,
    GET_ALL_PRODUCT_FAILURE,
  
    GET_AN_PRODUCT_SUCCESS,
    GET_AN_PRODUCT_FAILURE,
  
    CREATE_PRODUCT_SUCCESS,
    CREATE_PRODUCT_FAILURE
  } from "../config/ActionTypes";
  
  export function getAllProductSuccess(data) {
    return { type: GET_ALL_PRODUCT_SUCCESS, data: data };
  }
  export function getAllProductFailure(error) {
    return { type: GET_ALL_PRODUCT_FAILURE, error: error };
  }
  
  export function getAnProductSuccess(data) {
    return { type: GET_AN_PRODUCT_SUCCESS, data: data };
  }
  export function getAnProductFailure(error) {
    return { type: GET_AN_PRODUCT_FAILURE, error: error };
  }
  
  export function createProductSuccess(data){
    return { type: CREATE_PRODUCT_SUCCESS, data: data };
  }
  
  export function createProductFailure(error){
    return { type: CREATE_PRODUCT_FAILURE, error: error };
  }