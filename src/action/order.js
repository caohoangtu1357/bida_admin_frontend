import {
    GET_ALL_ORDER_SUCCESS,
    GET_ALL_ORDER_FAILURE,
  
    GET_AN_ORDER_SUCCESS,
    GET_AN_ORDER_FAILURE,
  
    CREATE_ORDER_SUCCESS,
    CREATE_ORDER_FAILURE
  } from "../config/ActionTypes";
  
  export function getAllOrderSuccess(data) {
    return { type: GET_ALL_ORDER_SUCCESS, data: data };
  }
  export function getAllOrderFailure(error) {
    return { type: GET_ALL_ORDER_FAILURE, error: error };
  }
  
  export function getAnOrderSuccess(data) {
    return { type: GET_AN_ORDER_SUCCESS, data: data };
  }
  export function getAnOrderFailure(error) {
    return { type: GET_AN_ORDER_FAILURE, error: error };
  }
  
  export function createOrderSuccess(data){
    return { type: CREATE_ORDER_SUCCESS, data: data };
  }
  
  export function createOrderFailure(error){
    return { type: CREATE_ORDER_FAILURE, error: error };
  }