import {
    GET_ALL_TABLE_TYPE_SUCCESS,
  
    GET_AN_TABLE_TYPE_SUCCESS,
  
    CREATE_TABLE_TYPE_SUCCESS,

  } from "../config/ActionTypes";
  
  export function getAllTableTypeSuccess(data) {
    return { type: GET_ALL_TABLE_TYPE_SUCCESS, data: data };
  }

  
  export function getAnTableTypeSuccess(data) {
    return { type: GET_AN_TABLE_TYPE_SUCCESS, data: data };
  }

  
  export function createTableTypeSuccess(data){
    return { type: CREATE_TABLE_TYPE_SUCCESS, data: data };
  }
  
