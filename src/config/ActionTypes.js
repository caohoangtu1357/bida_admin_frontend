export const GET_ALL_CATEGORY = "GET_ALL_CATEGORY";
export const GET_ALL_CATEGORY_SUCCESS = "GET_ALL_CATEGORY_SUCCESS";
export const GET_ALL_CATEGORY_FAILURE = "GET_ALL_CATEGORY_FAILURE";

export const UPDATE_CATEGORY = "UPDATE_CATEGORY";
export const UPDATE_CATEGORY_SUCCESS = "UPDATE_CATEGORY_SUCCESS";
export const UPDATE_CATEGORY_FAILURE = "UPDATE_CATEGORY_FAILURE";

export const GET_AN_CATEGORY = "GET_AN_CATEGORY";
export const GET_AN_CATEGORY_SUCCESS = "GET_AN_CATEGORY_SUCCESS";
export const GET_AN_CATEGORY_FAILURE = "GET_AN_CATEGORY_FAILURE";

export const ADD_CATEGORY = "ADD_CATEGORY";
export const ADD_CATEGORY_SUCCESS = "ADD_CATEGORY_SUCCESS";
export const ADD_CATEGORY_FAILURE = "ADD_CATEGORY_FAILURE";

export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const LOGOUT = "LOGOUT";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const CHECK_IS_LOGGED = "CHECK_IS_LOGGED";
export const CHECK_IS_LOGGED_SUCCESS = "CHECK_IS_LOGGED_SUCCESS";
export const CHECK_IS_LOGGED_FAILURE = "CHECK_IS_LOGGED_FAILURE";

export const DELETE_CATEGORY = "DELETE_CATEGORY";
export const DELETE_CATEGORY_SUCCESS = "DELETE_CATEGORY_SUCCESS";
export const DELETE_CATEGORY_FAILURE = "DELETE_CATEGORY_FAILURE";

export const GET_ALL_USER = "GET_ALL_USER";
export const GET_ALL_USER_SUCCESS = "GET_ALL_USER_SUCCESS";
export const GET_ALL_USER_FAILURE = "GET_ALL_USER_FAILURE";

export const GET_AN_USER = "GET_AN_USER";
export const GET_AN_USER_SUCCESS = "GET_AN_USER_SUCCESS";
export const GET_AN_USER_FAILURE = "GET_AN_USER_FAILURE";

export const DELETE_USER = "DELETE_USER";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
export const DELETE_USER_FAILURE = "DELETE_USER_FAILURE";

export const GET_LESSON_BY_ID_TABLE= "GET_LESSON_BY_ID_TABLE";
export const GET_LESSON_BY_ID_TABLE_SUCCESS= "GET_LESSON_BY_ID_TABLE_SUCCESS";
export const GET_LESSON_BY_ID_TABLE_FALSE= "GET_LESSON_BY_ID_TABLE_FALSE";

export const UPDATE_TABLE= "UPDATE_TABLE";

export const GRANT_ADMIN = "GRANT_ADMIN";

export const GET_ALL_TABLE_TYPE = "GET_ALL_TABLE_TYPE";
export const GET_ALL_TABLE_TYPE_SUCCESS = "GET_ALL_TABLE_TYPE_SUCCESS";
export const GET_ALL_TABLE_TYPE_FAILURE = "GET_ALL_TABLE_TYPE_FAILURE";

export const GET_AN_TABLE = "GET_AN_TABLE";
export const GET_AN_TABLE_SUCCESS = "GET_AN_TABLE_SUCCESS";
export const GET_AN_TABLE_FAILURE = "GET_AN_TABLE_FAILURE";

export const DELETE_TABLE = "DELETE_TABLE";
export const DELETE_TABLE_SUCCESS = "DELETE_TABLE_SUCCESS";
export const DELETE_TABLE_FAILURE = "DELETE_TABLE_FAILURE";

export const GET_ALL_TABLE = "GET_ALL_TABLE";
export const GET_ALL_TABLE_SUCCESS = "GET_ALL_TABLE_SUCCESS";
export const GET_ALL_TABLE_FAILURE = "GET_ALL_TABLE_FAILURE";

export const CREATE_TABLE = "CREATE_TABLE";
export const CREATE_TABLE_SUCCESS = "CREATE_TABLE_SUCCESS";
export const CREATE_TABLE_FAILURE = "CREATE_TABLE_FAILURE";

export const GET_AN_TABLE_TYPE = "GET_AN_TABLE_TYPE";
export const GET_AN_TABLE_TYPE_SUCCESS = "GET_AN_TABLE_TYPE_SUCCESS";



export const CREATE_TABLE_TYPE_SUCCESS = "CREATE_TABLE_TYPE_SUCCESS";
export const CREATE_TABLE_TYPE = "CREATE_TABLE_TYPE";

export const CREATE_ALERT="CREATE_ALERT";
export const STOP_ALERT="STOP_ALERT";

export const DELETE_TABLE_TYPE = "DELETE_TABLE_TYPE";


export const GET_AN_PRODUCT = "GET_AN_PRODUCT";
export const GET_AN_PRODUCT_SUCCESS = "GET_AN_PRODUCT_SUCCESS";
export const GET_AN_PRODUCT_FAILURE = "GET_AN_PRODUCT_FAILURE";

export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const DELETE_PRODUCT_SUCCESS = "DELETE_PRODUCT_SUCCESS";
export const DELETE_PRODUCT_FAILURE = "DELETE_PRODUCT_FAILURE";

export const GET_ALL_PRODUCT = "GET_ALL_PRODUCT";
export const GET_ALL_PRODUCT_SUCCESS = "GET_ALL_PRODUCT_SUCCESS";
export const GET_ALL_PRODUCT_FAILURE = "GET_ALL_PRODUCT_FAILURE";

export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const CREATE_PRODUCT_SUCCESS = "CREATE_PRODUCT_SUCCESS";
export const CREATE_PRODUCT_FAILURE = "CREATE_PRODUCT_FAILURE";

/**
 * Order
 */
export const GET_AN_ORDER = "GET_AN_ORDER";
export const GET_AN_ORDER_SUCCESS = "GET_AN_ORDER_SUCCESS";
export const GET_AN_ORDER_FAILURE = "GET_AN_ORDER_FAILURE";

export const UPDATE_ORDER = "UPDATE_ORDER";

export const DELETE_ORDER = "DELETE_ORDER";
export const DELETE_ORDER_SUCCESS = "DELETE_ORDER_SUCCESS";
export const DELETE_ORDER_FAILURE = "DELETE_ORDER_FAILURE";

export const GET_ALL_ORDER = "GET_ALL_ORDER";
export const GET_ALL_ORDER_SUCCESS = "GET_ALL_ORDER_SUCCESS";
export const GET_ALL_ORDER_FAILURE = "GET_ALL_ORDER_FAILURE";

export const CREATE_ORDER = "CREATE_ORDER";
export const CREATE_ORDER_SUCCESS = "CREATE_ORDER_SUCCESS";
export const CREATE_ORDER_FAILURE = "CREATE_ORDER_FAILURE";

export const DELETE_ATTRIBUTE = "DELETE_ATTRIBUTE";
export const DELETE_ATTRIBUTE_SUCCESS = "DELETE_ATTRIBUTE_SUCCESS";
export const DELETE_ATTRIBUTE_FAILURE = "DELETE_ATTRIBUTE_FAILURE";

export const GET_ALL_ATTRIBUTE = "GET_ALL_ATTRIBUTE";
export const GET_ALL_ATTRIBUTE_SUCCESS = "GET_ALL_ATTRIBUTE_SUCCESS";
export const GET_ALL_ATTRIBUTE_FAILURE = "GET_ALL_ATTRIBUTE_FAILURE";

export const CREATE_ATTRIBUTE = "CREATE_ATTRIBUTE";
export const CREATE_ATTRIBUTE_SUCCESS = "CREATE_ATTRIBUTE_SUCCESS";
export const CREATE_ATTRIBUTE_FAILURE = "CREATE_ATTRIBUTE_FAILURE";


export const DELETE_PRODUCT_TYPE = "DELETE_PRODUCT_TYPE";
export const DELETE_PRODUCT_TYPE_SUCCESS = "DELETE_PRODUCT_TYPE_SUCCESS";
export const DELETE_PRODUCT_TYPE_FAILURE = "DELETE_PRODUCT_TYPE_FAILURE";

export const GET_ALL_PRODUCT_TYPE = "GET_ALL_PRODUCT_TYPE";
export const GET_ALL_PRODUCT_TYPE_SUCCESS = "GET_ALL_PRODUCT_TYPE_SUCCESS";
export const GET_ALL_PRODUCT_TYPE_FAILURE = "GET_ALL_PRODUCT_TYPE_FAILURE";

export const CREATE_PRODUCT_TYPE = "CREATE_PRODUCT_TYPE";
export const CREATE_PRODUCT_TYPE_SUCCESS = "CREATE_PRODUCT_TYPE_SUCCESS";
export const CREATE_PRODUCT_TYPE_FAILURE = "CREATE_PRODUCT_TYPE_FAILURE";




