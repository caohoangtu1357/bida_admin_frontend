import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleOrder.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  GET_ALL_ORDER,
  DELETE_ORDER,
  CREATE_ORDER,
  UPDATE_ORDER
} from "../../config/ActionTypes";
import { url } from "../../config/API";

function Product(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [idTableType,setIdTableType] = useState("");

  useEffect(() => {
    props.getAllOrder();
  }, []);

  function deleteBtn(e) {
    props.deleteOrder(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
    setDescription("");
  }

  function createTable(e) {
    props.createTable({
      name:name,
      description:description,
      table_type_id: idTableType
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteOrder(e){
    props.deleteOrder(e.target.getAttribute("id"));
  }

  function updateOrder(e){
    props.updateOrder(e.target.getAttribute("id"));
  }

  function convertDate(dateInt){
    var d = new Date(dateInt);
    var ye = new Intl.DateTimeFormat('vi', { year: 'numeric' }).format(d);
    var mo = new Intl.DateTimeFormat('vi', { month: 'short' }).format(d);
    var da = new Intl.DateTimeFormat('vi', { day: '2-digit' }).format(d);
    return (`${da}-${mo}-${ye}`);
  }

  

  console.log(props.courses);
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm sản phẩm mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên sản phẩm</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại sản phẩm</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setIdTableType(e.target.value);}}>
                    <option disabled selected>Chọn loại sản phẩm</option>
                    {props.tableTypes?props.tableTypes.map((tableType, index) => {
                        return (
                          <option value={tableType._id}>{tableType.name}</option>
                        );
                      })
                    : ""}
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý hóa đơn</h2>
              </div>
            </Col>
            <Col xs="6">
          
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Ngày đặt</td>
                  <td>Trạng thái</td>
                  <td>yêu cầu thêm</td>
                  <td>Trị giá</td>
                  <td>Tên người đặt</td>
                  <td>Tên bàn</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.orders?props.orders.map((order, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{convertDate(parseInt(order.date_order))}</td>
                          <td>{order.status}</td>
                          <td>{order.requirement}</td>
                          <td>{order.cost}</td>
                          <td>{order.user_id.lastname+" "+order.user_id.firstname}</td>
                          <td>{order.table_id.name}</td>
                          <td>
                            <Button variant="success" id={order._id} onClick={updateOrder}>Xác nhận</Button>
                            <Button variant="danger" id={order._id} onClick={deleteOrder} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    orders: state.order.data.allOrder
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllOrder: () => dispatch({ type: GET_ALL_ORDER }),
    deleteOrder:(id) => dispatch({type:DELETE_ORDER,id:id}),
    updateOrder:(id) => dispatch({type:UPDATE_ORDER,id:id})
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
