import React,{useState,useEffect} from 'react';
import {Button,Container,Row,Col,Form,Alert} from 'react-bootstrap';
import './styleLogin.css';
import {connect} from 'react-redux';
import {LOGIN} from '../../config/ActionTypes';
import CustomAlert from "../../component/alert/alert.jsx";


function Login(props){

  const [username,setUsername] = useState("");
  const [password,setPassword] = useState("");
  const [visible, setVisible] = useState("none");


  useEffect(() => {

  },[]);




  function loginBtn(){
    props.login({username:username,password:password});
  }

  const onDismiss = () => setVisible(false);

  return(
    <Container fluid>
   
      <Row className="wrapper">
        <Col xs={12} className="col">
            <div className="main-login">
                <div className="login-box">
                    <div>
                        <CustomAlert variant="danger" />
                        <div className="heading">
                            Login
                        </div>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" onChange={(e)=>{setUsername(e.target.value)}} placeholder="Username" />
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlInput2">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" onChange={(e)=>{setPassword(e.target.value)}} placeholder="Password" />
                        </Form.Group>
                        <div style={{textAlign: "right"}}>
                            <Button variant="primary" onClick={loginBtn}>Login</Button>
                        </div>
                    </div>

                </div>
            </div>
        </Col>
      </Row>
    </Container>
  )
}

function mapStateToProps(state){
  return {
    error:state.authenticate.error
  }
}

function mapDispatchToProps(dispatch){
  return {
      login:(credential)=>dispatch({type:LOGIN,credential})
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);