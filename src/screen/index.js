import Dashboard from "./dashboard/Dashboard.jsx";
import Login from "./Login/Login.jsx";
import User from "./User/user.jsx";
import Table from "./table/table.jsx";
import TableType from "./tableType/tableType.jsx";
import Product from "./product/product.jsx";
import Order from "./order/order.jsx";
import Attribute from "./attribute/attribute.jsx";
import ProductType from "./productType/productType.jsx";

export { 
    Dashboard, 
    Login, 
    User, 
    Table,
    TableType,
    Product,
    Order,
    Attribute,
    ProductType
};
