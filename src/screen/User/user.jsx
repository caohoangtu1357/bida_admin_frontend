import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleUser.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  GET_ALL_USER,
  DELETE_USER
} from "../../config/ActionTypes";
import { url } from "../../config/API";

function Users(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [idUserType,setIdUserType] = useState("");

  useEffect(() => {
    props.getAllUser();
  }, []);

  function deleteBtn(e) {
    props.deleteUser(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
    setDescription("");
  }


  function deleteUser(e){
    props.deleteUser(e.target.getAttribute("id"));
  }

  

  console.log(props.courses);
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm người dùng mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên người dùng</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại người dùng</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setIdUserType(e.target.value);}}>
                    <option disabled selected>Chọn loại người dùng</option>
                    {props.tableTypes?props.tableTypes.map((tableType, index) => {
                        return (
                          <option value={tableType._id}>{tableType.name}</option>
                        );
                      })
                    : ""}
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý người dùng</h2>
              </div>
            </Col>
            <Col xs="6">
            
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Email</td>
                  <td>First name</td>
                  <td>Last name</td>
                  <td>phone</td>
                  <td>Quyền</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.users?props.users.map((user, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{user.email}</td>
                          <td>{user.firstname}</td>
                          <td>{user.lastname}</td>
                          <td>{user.phone}</td>
                          <td>{user.role}</td>
                          <td>
                            <Button variant="danger" id={user._id} onClick={deleteUser} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    users: state.user.data.allUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllUser: () => dispatch({ type: GET_ALL_USER }),
    deleteUser: (id) => dispatch({ type: DELETE_USER, id })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
