import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleAttribute.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  DELETE_ATTRIBUTE,
  CREATE_ATTRIBUTE,
  GET_ALL_ATTRIBUTE
} from "../../config/ActionTypes";
import { url } from "../../config/API";
import { set } from "date-fns/esm";

function Attribute(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [type,setType] = useState(0);
  const [quantity,setQuantity] = useState(0);

  useEffect(() => {
    props.getAllAttribute();
  }, []);

  function deleteBtn(e) {
    props.deleteTable(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
  }

  function createTable(e) {
    props.createAttribute({
      name:name,
      type:type
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteAttribute(e){
    props.deleteAttribute(e.target.getAttribute("id"));
  }

  

  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm thuộc tính mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên thuộc tính</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại thuộc tính</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setType(e.target.value);}}>
                    <option disabled selected>Chọn loại thuộc tính</option>
                    <option value="int">kiểu số</option>
                    <option value="string">kiểu văn bản</option>
                  </Form.Control>
                </Form.Group>
                
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý thuộc tính</h2>
              </div>
            </Col>
            <Col xs="6">
            <div style={{textAlign:"right",marginTop:15}}>
              <Button onClick={(e)=>{setModelVisibility("visible"); setModelOpacity(1);}} variant="warning">Thêm mới</Button>
            </div>
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Tên</td>
                  <td>Loại thuộc tính</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.attributes?props.attributes.map((tableType, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{tableType.name}</td>
                          <td>{tableType.type}</td>
                          <td>
                            <Button variant="danger" id={tableType._id} onClick={deleteAttribute} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    attributes: state.attribute.data.allAttribute,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteAttribute: (id) => dispatch({ type: DELETE_ATTRIBUTE, id }),
    createAttribute: (data)=>dispatch({type:CREATE_ATTRIBUTE,data}),
    getAllAttribute: ()=>dispatch({ type: GET_ALL_ATTRIBUTE }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Attribute);
