import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleProductType.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import axios from "axios";
import { connect } from "react-redux";
import {
  DELETE_PRODUCT_TYPE,
  CREATE_PRODUCT_TYPE,
  GET_ALL_PRODUCT_TYPE
} from "../../config/ActionTypes";
import { url } from "../../config/API";
import { set } from "date-fns/esm";

function ProductType(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [price,setPrice] = useState(0);
  const [quantity,setQuantity] = useState(0);
  const [image,setImage] = useState("");
  const [imageUploaded,setImageUploaded] = useState("");

  useEffect(() => {
    props.getAllProductType();
  }, []);

  function deleteBtn(e) {
    props.deleteTable(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
    setDescription("");
    setQuantity(0);
    setPrice(0);
    setImageUploaded("");
    setImage("");
  }

  function createTable(e) {
    props.createProductType({
      name:name,
      description:description,
      quantity:quantity,
      price:price,
      image:imageUploaded
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteProductType(e){
    props.deleteProductType(e.target.getAttribute("id"));
  }

  function uploadImage(data){
    console.log("asdad",data);
    let dataForm = new FormData();
    dataForm.append('image', data);
    return axios.post(url+"/image/upload",dataForm,{ 
        headers: {
            'content-type': `multipart/form-data;`
        }
    }).then(res=>{
      console.log(res.data);
      setImageUploaded(res.data.uploaded);
    }).catch(err=>{
        return console.log(err.response.data.message);
    });
  }
  
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm loại sản phẩm mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên loại sản phẩm</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <Form.Control type="file" value={image} onChange={(e)=>{setImage(e.target.value);uploadImage(e.target.files[0])}}/>
                  <img style={{width: 98, marginTop: 8}} src={url+"/"+imageUploaded}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Số lượng bàn</Form.Label>
                  <Form.Control type="number" value={quantity} onChange={(e)=>{setQuantity(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Đơn giá</Form.Label>
                  <Form.Control type="number" value={price} onChange={(e)=>{setPrice(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý loại sản phẩm</h2>
              </div>
            </Col>
            <Col xs="6">
            <div style={{textAlign:"right",marginTop:15}}>
              <Button onClick={(e)=>{setModelVisibility("visible"); setModelOpacity(1);}} variant="warning">Thêm mới</Button>
            </div>
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Tên</td>
                  <td>Hình ảnh</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.productTypes?props.productTypes.map((productType, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{productType.name}</td>
                          <td><img style={{width: 150}} src={url+"/"+productType.image} alt=""/></td>
                          <td>
                            <Button variant="danger" id={productType._id} onClick={deleteProductType} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    productTypes: state.productType.data.allProductType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteProductType: (id) => dispatch({ type: DELETE_PRODUCT_TYPE, id }),
    createProductType: (data)=>dispatch({type:CREATE_PRODUCT_TYPE,data}),
    getAllProductType: ()=>dispatch({ type: GET_ALL_PRODUCT_TYPE }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductType);
