import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleTable.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  GET_ALL_TABLE,
  DELETE_TABLE,
  CREATE_TABLE,
  GET_ALL_TABLE_TYPE,
  UPDATE_TABLE
} from "../../config/ActionTypes";
import { url } from "../../config/API";

function Tables(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [idTableType,setIdTableType] = useState("");

  useEffect(() => {
    props.getAllTable();
    props.getAllTableType();
  }, []);

  function deleteBtn(e) {
    props.deleteTable(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
    setDescription("");
  }

  function createTable(e) {
    props.createTable({
      name:name,
      description:description,
      table_type_id: idTableType
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteTable(e){
    props.deleteTable(e.target.getAttribute("id"));
  }

  function updateTable(e){
    props.updateTable(e.target.getAttribute("id"));
  }

  

  console.log(props.courses);
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm bàn mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên bàn</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại bàn</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setIdTableType(e.target.value);}}>
                    <option disabled selected>Chọn loại bàn</option>
                    {props.tableTypes?props.tableTypes.map((tableType, index) => {
                        return (
                          <option value={tableType._id}>{tableType.name}</option>
                        );
                      })
                    : ""}
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý Bàn</h2>
              </div>
            </Col>
            <Col xs="6">
            <div style={{textAlign:"right",marginTop:15}}>
              <Button onClick={(e)=>{setModelVisibility("visible"); setModelOpacity(1);}} variant="warning">Thêm bàn mới</Button>
            </div>
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Tên</td>
                  <td>Mô tả</td>
                  <td>Loại bàn</td>
                  <td>Trạng thái</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.tables?props.tables.map((table, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{table.name}</td>
                          <td>{table.description}</td>
                          <td>{table.table_type_id.name}</td>
                          <td>{table.status}</td>
                          <td>
                            <Button variant="success" id={table._id} onClick={updateTable}>Chuyển trạng thái</Button>
                            <Button variant="danger" id={table._id} onClick={deleteTable} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    tables: state.table.data.allTable,
    tableTypes: state.tableType.data.allTableType
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllTable: () => dispatch({ type: GET_ALL_TABLE }),
    deleteTable: (id) => dispatch({ type: DELETE_TABLE, id }),
    createTable: (data)=>dispatch({type:CREATE_TABLE,data}),
    getAllTableType: ()=>dispatch({ type: GET_ALL_TABLE_TYPE }),
    updateTable: (id) => dispatch({type: UPDATE_TABLE,id:id})
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Tables);
