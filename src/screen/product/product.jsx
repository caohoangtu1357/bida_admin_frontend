import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleProduct.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import axios from "axios";
import {url} from "../../config/API";
import {
  GET_ALL_PRODUCT,
  DELETE_PRODUCT,
  CREATE_PRODUCT,
  GET_ALL_PRODUCT_TYPE,
  GET_ALL_ATTRIBUTE
} from "../../config/ActionTypes";

function Product(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [idProductType,setIdProductType] = useState("");
  const [qty,setQty] = useState(0);
  const [image,setImage] = useState("");
  const [imageUploaded,setImageUploaded] = useState("");
  const [modelAttributeVisibility,setModelAttributeVisibility]=useState("hidden");
  const [modelAttributeOpacity,setModelAttributeOpacity]=useState(0);
  const [productId,setProductId]=useState("");
  const [value,setValue]=useState("");
  const [attributeId,setAttributeId]= useState("");

  useEffect(() => {
    props.getAllProduct();
    props.getAllProductType();
    props.getAllAttribute();
  }, []);

  function deleteBtn(e) {
    props.deleteProduct(e.target.getAttribute("idcourse"));
  }
  

  function createEAV(){
    return axios.post(url+"/product-attribute-value/create",{
      product_id:productId,
      product_attribute_id:attributeId,
      value:value
    }).then(res=>{
      setModelAttributeVisibility("hidden");
      setModelAttributeOpacity(0);
      resetForm();
    }).catch(err=>{
        return console.log(err.response.data.message);
    });
  }

  function uploadImage(data){
    console.log("asdad",data);
    let dataForm = new FormData();
    dataForm.append('image', data);
    return axios.post(url+"/image/upload",dataForm,{ 
        headers: {
            'content-type': `multipart/form-data;`
        }
    }).then(res=>{
      console.log(res.data);
      setImageUploaded(res.data.uploaded);
    }).catch(err=>{
        return console.log(err.response.data.message);
    });
  }

  function resetForm(){
    setName("");
    setDescription("");
    setQty(0);
  }

  function createTable(e) {
    props.createProduct({
      name:name,
      description:description,
      quantity:qty,
      image:imageUploaded,
      product_type_id: idProductType
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteProduct(e){
    props.deleteProduct(e.target.getAttribute("id"));
  }

  

  console.log(props.courses);
  return (
    <Container fluid style={{overflow: "auto"}}>
      <Row className="wrapper" style={{minHeight: "900px"}}>
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />


          <div className="model" style={{visibility:modelAttributeVisibility, opacity:modelAttributeOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm thuộc tính mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Chọn thuộc tính</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setAttributeId(e.target.value);}}>
                    <option disabled selected>Chọn thuộc tính</option>
                    {props.attributes?props.attributes.map((attribute, index) => {
                        return (
                          <option value={attribute._id}>{attribute.name}</option>
                        );
                      })
                    : ""}
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>giá trị</Form.Label>
                  <Form.Control type="text" value={value} onChange={(e)=>{setValue(e.target.value);}}/>
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createEAV}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelAttributeVisibility("hidden"); setModelAttributeOpacity(0)}}>Đóng</Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </div>


          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm sản phẩm mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên sản phẩm</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại sản phẩm</Form.Label>
                  <Form.Control as="select" onChange={(e)=>{setIdProductType(e.target.value);}}>
                    <option disabled selected>Chọn loại sản phẩm</option>
                    {props.productTypes?props.productTypes.map((productType, index) => {
                        return (
                          <option value={productType._id}>{productType.name}</option>
                        );
                      })
                    : ""}
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Số lượng</Form.Label>
                  <Form.Control type="number" value={qty} onChange={(e)=>{setQty(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <Form.Control type="file" value={image} onChange={(e)=>{setImage(e.target.value);uploadImage(e.target.files[0])}}/>
                  <img style={{width: 98, marginTop: 8}} src={url+"/"+imageUploaded}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý kho hàng</h2>
              </div>
            </Col>
            <Col xs="6">
            <div style={{textAlign:"right",marginTop:15}}>
              <Button onClick={(e)=>{setModelVisibility("visible"); setModelOpacity(1);}} variant="warning">Thêm sản phẩm mới</Button>
            </div>
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Ảnh</td>
                  <td>Tên</td>
                  <td>Mô tả</td>
                  <td>Số lượng</td>
                  <td>Loại sản phẩm</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.products?props.products.map((product, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td><img style={{width: 98, marginTop: 8}} src={url+"/"+product.image}/></td>
                          <td>{product.name}</td>
                          <td>{product.description}</td>
                          <td>{product.quantity}</td>
                          <td>{product.product_type_id.name}</td>
                          <td>
                            <Button variant="success" id={product._id} onClick={(e)=>{setModelAttributeVisibility("visible"); setModelAttributeOpacity(1);setProductId(e.target.getAttribute('id'));}}>Thêm thuộc tính</Button>
                            <Button variant="danger" id={product._id} onClick={deleteProduct} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    products: state.product.data.allProduct,
    tableTypes: state.tableType.data.allTableType,
    productTypes: state.productType.data.allProductType,
    attributes: state.attribute.data.allAttribute
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllProduct: () => dispatch({ type: GET_ALL_PRODUCT }),
    deleteProduct: (id) => dispatch({ type: DELETE_PRODUCT, id }),
    createProduct: (data)=>dispatch({type:CREATE_PRODUCT,data}),
    getAllProductType: ()=>dispatch({ type: GET_ALL_PRODUCT_TYPE }),
    getAllAttribute: ()=>dispatch({ type: GET_ALL_ATTRIBUTE })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
