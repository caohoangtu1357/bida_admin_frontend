import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
  FormGroup
  
} from "react-bootstrap";
import "./styleTableType.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  DELETE_TABLE_TYPE,
  CREATE_TABLE_TYPE,
  GET_ALL_TABLE_TYPE
} from "../../config/ActionTypes";
import { url } from "../../config/API";
import { set } from "date-fns/esm";

function TableType(props) {

  const [modelVisibility,setModelVisibility]=useState("hidden");
  const [modelOpacity,setModelOpacity]=useState(0);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [price,setPrice] = useState(0);
  const [quantity,setQuantity] = useState(0);

  useEffect(() => {
    props.getAllTableType();
  }, []);

  function deleteBtn(e) {
    props.deleteTable(e.target.getAttribute("idcourse"));
  }

  function resetForm(){
    setName("");
    setDescription("");
    setQuantity(0);
    setPrice(0);
  }

  function createTable(e) {
    props.createTableType({
      name:name,
      description:description,
      quantity:quantity,
      price:price
    });
    setModelVisibility("hidden");
    setModelOpacity(0);
    resetForm();
  }

  function deleteTableType(e){
    props.deleteTableType(e.target.getAttribute("id"));
  }

  

  console.log(props.courses);
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <div className="model" style={{visibility:modelVisibility, opacity:modelOpacity}}>
            <div>
              <Form style={{padding: "1rem 2rem 1rem 2rem"}}>
                <div>
                  <h2>Thêm loại bàn mới</h2>
                </div>
                <Form.Group>
                  <Form.Label>Tên loại bàn</Form.Label>
                  <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Số lượng bàn</Form.Label>
                  <Form.Control type="number" value={quantity} onChange={(e)=>{setQuantity(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Đơn giá</Form.Label>
                  <Form.Control type="number" value={price} onChange={(e)=>{setPrice(e.target.value);}}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control value={description} onChange={(e)=>{setDescription(e.target.value);}} as="textarea" rows={3} />
                </Form.Group>
                <Row style={{textAlign:"right", marginBottom:"1rem"}}>
                  <Col xs={6} style={{padding:0}}>
                    <Button onClick={createTable}>Thêm mới</Button>
                  </Col>
                  <Col xs={6} style={{padding:0}}>
                    <Button variant="danger" onClick={(e)=>{setModelVisibility("hidden"); setModelOpacity(0)}}>Đóng</Button>
                  </Col>
              </Row>
              </Form>
            </div>
          </div>

          <Row className="main-content">
            <Col xs="6">
              <div>
                <h2 style={{textAlign:"left"}} className="heading">Quản lý loại bàn</h2>
              </div>
            </Col>
            <Col xs="6">
            <div style={{textAlign:"right",marginTop:15}}>
              <Button onClick={(e)=>{setModelVisibility("visible"); setModelOpacity(1);}} variant="warning">Thêm mới</Button>
            </div>
            </Col>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Tên</td>
                  <td>Mô tả</td>
                  <td>Số lượng</td>
                  <td>Đơn giá</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.tableTypes?props.tableTypes.map((tableType, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{tableType.name}</td>
                          <td>{tableType.description}</td>
                          <td>{tableType.quantity}</td>
                          <td>{tableType.price}</td>
                          <td>
                            <Button variant="danger" id={tableType._id} onClick={deleteTableType} style={{marginLeft:"1rem"}} >Xóa</Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    tables: state.table.data.allTable,
    tableTypes: state.tableType.data.allTableType
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteTableType: (id) => dispatch({ type: DELETE_TABLE_TYPE, id }),
    createTableType: (data)=>dispatch({type:CREATE_TABLE_TYPE,data}),
    getAllTableType: ()=>dispatch({ type: GET_ALL_TABLE_TYPE }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TableType);
