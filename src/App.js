import React, { useEffect } from "react";
import { Redirect, Route, HashRouter as Router } from "react-router-dom";
import {Login, User,Table,Dashboard, TableType, Product,Order, ProductType, Attribute} from "./screen/index.js";
import { connect } from "react-redux";
import { CHECK_IS_LOGGED } from "./config/ActionTypes";

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.checkIsLogged();
  }

  render() {
    return (
      <Router>
        <div>
          <Route
            exact
            path="/"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Dashboard />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/revenue"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Table />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/table"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Table />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/table-type"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <TableType />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route 
            exact
            path="/product"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Product />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route 
            exact
            path="/user"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <User />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route
            exact
            path="/order"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Order />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/product-type"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <ProductType />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/attribute"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Attribute />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route exact path="/login" component={Login} />
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.authenticate.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    checkIsLogged: () => dispatch({ type: CHECK_IS_LOGGED }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
