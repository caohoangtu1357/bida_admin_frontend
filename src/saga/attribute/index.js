import { all } from "redux-saga/effects";
import { watchGetAllAttribute } from "./read";
import { watchDeleteAttribute } from "./delete";
import {watchCreateAttribute} from "./create";

export default function* rootAttribute() {
  yield all([
    watchGetAllAttribute(),
    watchDeleteAttribute(),
    watchCreateAttribute()
  ]);
}
