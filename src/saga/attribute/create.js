import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_ATTRIBUTE, GET_ALL_ATTRIBUTE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function createAsync(data) {
  return axios
    .post(url + "/product-attribute/create",data)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createAttribute(data) {
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_ATTRIBUTE });
}

export function* watchCreateAttribute() {
  yield takeLatest(CREATE_ATTRIBUTE, createAttribute);
}
