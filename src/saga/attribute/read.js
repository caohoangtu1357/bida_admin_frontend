import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_ATTRIBUTE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllAttributeSuccess,
  getAnAttributeSuccess,
} from "../../action/attribute";

function readAsync() {
  return axios
    .get(url + "/product-attribute/gets")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllAttributeSuccess(data));
  } catch (err) {
    console.log(err);
  }
}

export function* watchGetAllAttribute() {
  yield takeLatest(GET_ALL_ATTRIBUTE, read);
}

// function readByIdAsync(idTable) {
//   return axios
//     .get(url + "product-attribute/get/" + idTable)
//     .then((res) => {
//       return Promise.resolve(res.data);
//     })
//     .catch((err) => {
//       return Promise.reject(err);
//     });
// }

// function* readById(action) {
//   try {
//     var data = yield call(readByIdAsync, action.id);
//     yield put(getAnAttributeSuccess(data));
//   } catch (error) {
//     console.log(error);
//   }
// }

// export function* watchGetAnAttributeById() {
//   yield takeLatest(GET_AN_ATTRIBUTE, readById);
// }
