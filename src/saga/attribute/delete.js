import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_ATTRIBUTE, GET_ALL_ATTRIBUTE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function deleteAsync(id) {
  return axios
    .delete(url + "/product-attribute/delete/"+id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteAttribute(data) {
  yield call(deleteAsync, data);
  yield put({ type: GET_ALL_ATTRIBUTE });
}

export function* watchDeleteAttribute() {
  yield takeLatest(DELETE_ATTRIBUTE, deleteAttribute);
}
