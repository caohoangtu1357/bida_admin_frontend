import { all } from "redux-saga/effects";
import { watchGetAnTableById, watchGetAllTable } from "./read";
import { watchDeleteTable } from "./delete";
import {watchCreateTable} from "./create";
import {watchUpdateTable} from "./update"

export default function* rootShoes() {
  yield all([
    watchGetAnTableById(),
    watchGetAllTable(),
    watchDeleteTable(),
    watchCreateTable(),
    watchUpdateTable()
  ]);
}
