import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_TABLE, GET_AN_TABLE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllTableFailure,
  getAllTableSuccess,
  getAnTableFailure,
  getAnTableSuccess,
} from "../../action/table";

function readAsync() {
  return axios
    .get(url + "/table/gets")
    .then((res) => {
      console.log(res.data);
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllTableSuccess(data));
  } catch (err) {
    yield put(getAllTableFailure(err));
  }
}

export function* watchGetAllTable() {
  yield takeLatest(GET_ALL_TABLE, read);
}

function readByIdAsync(idTable) {
  return axios
    .get(url + "table/get/" + idTable)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnTableSuccess(data));
  } catch (error) {
    yield put(getAnTableFailure(error));
  }
}

export function* watchGetAnTableById() {
  yield takeLatest(GET_AN_TABLE, readById);
}
