import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_TABLE, GET_ALL_TABLE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function createAsync(data) {
  return axios
    .post(url + "/table/create",{
      name:data.name,
      description:data.description,
      table_type_id:data.table_type_id
    })
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createTable(data) {
  console.log(data);
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_TABLE });
}

export function* watchCreateTable() {
  yield takeLatest(CREATE_TABLE, createTable);
}
