import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_TABLE, GET_ALL_TABLE,UPDATE_TABLE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function deleteAsync(id) {
  return axios
    .put(url + "/table/update/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteTable(id) {
  yield call(deleteAsync, id);
  yield put({ type: GET_ALL_TABLE });
}

export function* watchUpdateTable() {
  yield takeLatest(UPDATE_TABLE, deleteTable);
}
