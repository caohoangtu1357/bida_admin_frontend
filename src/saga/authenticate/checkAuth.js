import {takeLatest,call,put} from 'redux-saga/effects';
import {url} from '../../config/API';
import {CHECK_IS_LOGGED} from '../../config/ActionTypes';
import axios from 'axios';


import {
    checkIsLoggedSuccess,
    checkIsLoggedFailure
} from '../../action/authenticate';

function checkAsync(){
    return axios.get(url+"/is-logged").then(res=>{
        return Promise.resolve(res.data);
    }).catch(err=>{
        return Promise.reject(err.response.data);
    });
}

function* checkAuth(){
    try{
        var data= yield call(checkAsync);
        yield put(checkIsLoggedSuccess(data));
    }catch(err){
        yield put(checkIsLoggedFailure(err));
    }
}

export function* watchCheckIsLogged(){
    yield takeLatest(CHECK_IS_LOGGED,checkAuth);
}
