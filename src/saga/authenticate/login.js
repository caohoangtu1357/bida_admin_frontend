import {takeLatest,call,put} from 'redux-saga/effects';
import {url} from '../../config/API';
import {LOGIN} from '../../config/ActionTypes';
import axios from 'axios';


import {
    loginSuccess,
    loginFailure
} from '../../action/authenticate';

import {
    addAlert,
    stopAlert
}from '../../action/alert';

function loginAsync(data){
    return axios.post(url+"/login",{username:data.username,password:data.password},).then(res=>{
        var data=res.data;
        data["auth-token"]=res.headers['auth-token'];
        return Promise.resolve(data);
    }).catch(err=>{
        return Promise.reject(err.response.data.message);
    });
}
const delay = time => new Promise(resolve => setTimeout(resolve, time));

function* login(actionData){
    try{
        var data= yield call(loginAsync,actionData.credential);
        localStorage.setItem("auth-token",data["auth-token"]);
        yield put(loginSuccess(data));
    }catch(err){
        yield put(addAlert(err));
        yield call(delay,5000);
        yield put(stopAlert());
    }
}

export function* watchLogin(){
    yield takeLatest(LOGIN,login);
}
