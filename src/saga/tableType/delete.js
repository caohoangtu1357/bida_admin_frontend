import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_TABLE_TYPE, GET_ALL_TABLE_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function deleteAsync(id) {
  return axios
    .delete(url + "/table-type/delete/"+id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteTableType(data) {
  yield call(deleteAsync, data);
  yield put({ type: GET_ALL_TABLE_TYPE });
}

export function* watchDeleteTableType() {
  yield takeLatest(DELETE_TABLE_TYPE, deleteTableType);
}
