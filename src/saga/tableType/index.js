import { all } from "redux-saga/effects";
import { watchGetAnTableTypeById, watchGetAllTableType } from "./read";
import { watchDeleteTableType } from "./delete";
import {watchCreateTableType} from "./create";

export default function* rootTableType() {
  yield all([
    watchGetAnTableTypeById(),
    watchGetAllTableType(),
    watchDeleteTableType(),
    watchCreateTableType()
  ]);
}
