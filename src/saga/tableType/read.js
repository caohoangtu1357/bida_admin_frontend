import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_TABLE_TYPE, GET_AN_TABLE_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllTableTypeSuccess,
  getAnTableTypeSuccess,
} from "../../action/tableType";

function readAsync() {
  return axios
    .get(url + "/table-type/gets")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllTableTypeSuccess(data));
  } catch (err) {
    console.log(err);
  }
}

export function* watchGetAllTableType() {
  yield takeLatest(GET_ALL_TABLE_TYPE, read);
}

function readByIdAsync(idTable) {
  return axios
    .get(url + "table-type/get/" + idTable)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnTableTypeSuccess(data));
  } catch (error) {
    console.log(error);
  }
}

export function* watchGetAnTableTypeById() {
  yield takeLatest(GET_AN_TABLE_TYPE, readById);
}
