import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_TABLE_TYPE, GET_ALL_TABLE_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function createAsync(data) {
  return axios
    .post(url + "/table-type/create",data)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createTableType(data) {
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_TABLE_TYPE });
}

export function* watchCreateTableType() {
  yield takeLatest(CREATE_TABLE_TYPE, createTableType);
}
