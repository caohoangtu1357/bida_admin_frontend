import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_USER, GET_AN_USER } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllUserFailure,
  getAllUserSuccess,
  getAnUserFailure,
  getAnUserSuccess,
} from "../../action/user";

function readAsync() {
  return axios
    .get(url + "/user/gets")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllUserSuccess(data));
  } catch (err) {
    yield put(getAllUserFailure(err));
  }
}

export function* watchGetAllUser() {
  yield takeLatest(GET_ALL_USER, read);
}
