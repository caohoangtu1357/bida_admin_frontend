import { all } from "redux-saga/effects";
import { watchGetAllUser } from "./read";
import { watchDeleteUser } from "./delete";
import { watchGrantadmin } from "./grant";

export default function* rootShoes() {
  yield all([watchGetAllUser(), watchDeleteUser(), watchGrantadmin()]);
}
