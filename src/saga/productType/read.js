import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_PRODUCT_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllProductTypeSuccess
} from "../../action/productType";

function readAsync() {
  return axios
    .get(url + "/product-type/gets")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllProductTypeSuccess(data));
  } catch (err) {
    console.log(err);
  }
}

export function* watchGetAllProductType() {
  yield takeLatest(GET_ALL_PRODUCT_TYPE, read);
}

// function readByIdAsync(idTable) {
//   return axios
//     .get(url + "product-type/get/" + idTable)
//     .then((res) => {
//       return Promise.resolve(res.data);
//     })
//     .catch((err) => {
//       return Promise.reject(err);
//     });
// }

// function* readById(action) {
//   try {
//     var data = yield call(readByIdAsync, action.id);
//     yield put(getAnProductTypeSuccess(data));
//   } catch (error) {
//     console.log(error);
//   }
// }

// export function* watchGetAnProductTypeById() {
//   yield takeLatest(GET_AN_PRODUCT_TYPE, readById);
// }
