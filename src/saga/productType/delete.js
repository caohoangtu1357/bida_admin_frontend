import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_PRODUCT_TYPE, GET_ALL_PRODUCT_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function deleteAsync(id) {
  return axios
    .delete(url + "/product-type/delete/"+id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteProductType(data) {
  yield call(deleteAsync, data);
  yield put({ type: GET_ALL_PRODUCT_TYPE });
}

export function* watchDeleteProductType() {
  yield takeLatest(DELETE_PRODUCT_TYPE, deleteProductType);
}
