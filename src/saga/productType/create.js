import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_PRODUCT_TYPE, GET_ALL_PRODUCT_TYPE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/table";

function createAsync(data) {
  return axios
    .post(url + "/product-type/create",data)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createProductType(data) {
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_PRODUCT_TYPE });
}

export function* watchCreateProductType() {
  yield takeLatest(CREATE_PRODUCT_TYPE, createProductType);
}
