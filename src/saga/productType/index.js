import { all } from "redux-saga/effects";
import { watchGetAllProductType } from "./read";
import { watchDeleteProductType } from "./delete";
import {watchCreateProductType} from "./create";

export default function* rootProductType() {
  yield all([
    watchGetAllProductType(),
    watchDeleteProductType(),
    watchCreateProductType()
  ]);
}
