import { all } from "redux-saga/effects";
import { watchGetAnProductById, watchGetAllProduct } from "./read";
import { watchDeleteProduct } from "./delete";
import {watchCreateProduct} from "./create";

export default function* rootShoes() {
  yield all([
    watchGetAnProductById(),
    watchGetAllProduct(),
    watchDeleteProduct(),
    watchCreateProduct()
  ]);
}
