import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_PRODUCT, GET_AN_PRODUCT } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllProductFailure,
  getAllProductSuccess,
  getAnProductFailure,
  getAnProductSuccess,
} from "../../action/product";

function readAsync() {
  return axios
    .get(url + "/product/gets")
    .then((res) => {
      console.log(res.data);
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllProductSuccess(data));
  } catch (err) {
    yield put(getAllProductFailure(err));
  }
}

export function* watchGetAllProduct() {
  yield takeLatest(GET_ALL_PRODUCT, read);
}

function readByIdAsync(idProduct) {
  return axios
    .get(url + "product/get/" + idProduct)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnProductSuccess(data));
  } catch (error) {
    yield put(getAnProductFailure(error));
  }
}

export function* watchGetAnProductById() {
  yield takeLatest(GET_AN_PRODUCT, readById);
}
