import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_PRODUCT, GET_ALL_PRODUCT } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/product";

function createAsync(data) {
  return axios
    .post(url + "/product/create",{
      name:data.name,
      description:data.description,
      quantity:data.quantity,
      image:data.image,
      product_type_id: data.product_type_id
    })
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createProduct(data) {
  console.log(data);
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_PRODUCT });
}

export function* watchCreateProduct() {
  yield takeLatest(CREATE_PRODUCT, createProduct);
}
