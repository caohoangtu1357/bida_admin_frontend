import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_PRODUCT, GET_ALL_PRODUCT } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/product";

function deleteAsync(id) {
  return axios
    .delete(url + "/product/delete/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteProduct(id) {
  yield call(deleteAsync, id);
  yield put({ type: GET_ALL_PRODUCT });
}

export function* watchDeleteProduct() {
  yield takeLatest(DELETE_PRODUCT, deleteProduct);
}
