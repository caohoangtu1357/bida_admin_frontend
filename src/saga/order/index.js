import { all } from "redux-saga/effects";
import { watchGetAnOrderById, watchGetAllOrder } from "./read";
import { watchDeleteOrder } from "./delete";
import {watchCreateOrder} from "./create";
import {watchUpdateOrder} from "./update";

export default function* rootShoes() {
  yield all([
    watchGetAnOrderById(),
    watchGetAllOrder(),
    watchDeleteOrder(),
    watchCreateOrder(),
    watchUpdateOrder()
  ]);
}
