import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { CREATE_ORDER, GET_ALL_ORDER } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/order";

function createAsync(data) {
  return axios
    .post(url + "/order/create",{
      name:data.name,
      description:data.description,
      order_type_id:data.order_type_id
    })
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* createOrder(data) {
  console.log(data);
  yield call(createAsync, data.data);
  yield put({ type: GET_ALL_ORDER });
}

export function* watchCreateOrder() {
  yield takeLatest(CREATE_ORDER, createOrder);
}
