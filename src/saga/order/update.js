import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { UPDATE_ORDER, GET_ALL_ORDER } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/order";

function deleteAsync(id) {
  return axios
    .put(url + "/order/update/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteOrder(id) {
  yield call(deleteAsync, id);
  yield put({ type: GET_ALL_ORDER });
}

export function* watchUpdateOrder() {
  yield takeLatest(UPDATE_ORDER, deleteOrder);
}
