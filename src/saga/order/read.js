import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_ORDER, GET_AN_ORDER } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllOrderFailure,
  getAllOrderSuccess,
  getAnOrderFailure,
  getAnOrderSuccess,
} from "../../action/order";

function readAsync() {
  return axios
    .get(url + "/order/gets")
    .then((res) => {
      console.log(res.data);
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllOrderSuccess(data));
  } catch (err) {
    yield put(getAllOrderFailure(err));
  }
}

export function* watchGetAllOrder() {
  yield takeLatest(GET_ALL_ORDER, read);
}

function readByIdAsync(idOrder) {
  return axios
    .get(url + "order/get/" + idOrder)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnOrderSuccess(data));
  } catch (error) {
    yield put(getAnOrderFailure(error));
  }
}

export function* watchGetAnOrderById() {
  yield takeLatest(GET_AN_ORDER, readById);
}
