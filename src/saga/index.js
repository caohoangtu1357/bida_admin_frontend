import { all } from "redux-saga/effects";
import rootUser from "./user/index";
import rootAuthenticate from "./authenticate/index";
import rootTable from "./table/index";
import rootTableType from "./tableType/index";
import rootProduct from "./product/index";
import rootOrder from "./order/index";
import rootAttribute from "./attribute/index";
import rootProductType from "./productType/index";

export default function* rootSaga() {
  yield all([
    rootAuthenticate(), 
    rootUser(), 
    rootTable(),
    rootTableType(),
    rootProduct(),
    rootOrder(),
    rootAttribute(),
    rootProductType()
  ]);
}
