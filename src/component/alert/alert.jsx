import React,{useState,useEffect} from 'react';
import {Button,Container,Row,Col,Form,Alert} from 'react-bootstrap';
// import './styleLogin.css';
import {connect} from 'react-redux';
import {LOGIN} from '../../config/ActionTypes';


function CustomAlert(props){

  const [username,setUsername] = useState("");
  const [password,setPassword] = useState("");
  const [visible, setVisible] = useState("none");



  useEffect(() => {

  },[]);

  return(
    <div style={{width:"100%"}}>
        <div style={{display:props.alert.message?"block":"none"}}>
          <Alert variant={props.variant}>{props.alert.message}</Alert>
        </div>
    </div>
  )
}

function mapStateToProps(state){
  return {
    alert:state.alert
  }
}

function mapDispatchToProps(dispatch){
  return {
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CustomAlert);