import React,{useState} from 'react';
import {Navbar,Nav,FormControl,Button,Form} from 'react-bootstrap';
import './menu.css';
import { connect } from "react-redux";
import {faSignOutAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {url} from "../../config/API.js";
import {
  LOGOUT
} from "../../config/ActionTypes";



function Menu(props){
  const [display,setDisplay]=useState('none');
  function disPlayModel(){
    setDisplay('block');
  };

  function logoutItem(){
    props.logout();
  }

  console.log(props.user);

  return(
    <div>
      <div>
        <Navbar style = {{ background : '#1167aa'}}>    
          <div id="idLogoutModel">
            <img src={url + "/upload/user_image/" + props.user.image} className="avatar" alt=""/>
            <div className="logoutModel">
              <p className="logout-model-item" onClick={logoutItem}><span><FontAwesomeIcon icon={faSignOutAlt}/> Logout</span></p>
            </div>
          </div>
        </Navbar>
      </div>
      
    </div>
    
  )
}

function mapStateToProps(state) {
  return {
    user: state.authenticate.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch({ type: LOGOUT })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);