import React, { useState } from "react";
import "./SideBar.css";
import './style.scss';
import { Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGlobeAsia,
  faBookReader,
  faBookMedical,
  faUser,
  faAmbulance,
  faBars,
  faGraduationCap,
  faListUl,
  faBowlingBall
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function SideBar(props) {
 
  return (
    <div className="side-bar-wrapper" >
     {/*  <span onClick = { toggleSidebar } className = 'toggle-sidebar'><FontAwesomeIcon icon = { faBars } /></span> */}
      <div className="sidebar-header" >
        <h3>
          <span>
            <FontAwesomeIcon icon={faBowlingBall} size="2x" /> Bida Tac Phong
          </span>
        </h3>
      </div>
      <Nav className="nav-wrapper">
        <Nav.Link>
          <Link to="/">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Tổng quan
            </span>
          </Link>
        </Nav.Link>
        {/* <Nav.Link>
          <Link to="/revenue">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Doang Thu
            </span>
          </Link>
        </Nav.Link> */}
        <Nav.Link>
          <Link to="/table">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} /> 
              Quản Lý Bàn
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/table-type">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} /> 
              Quản Lý Loại Bàn
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/product">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Quản Lý Kho
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/user">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Người Dùng
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/order">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Quản Lý Hóa Đơn
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/product-type">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              QL Loại Sản Phẩm
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/attribute">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faListUl} />{" "}
              Quản Lý thuộc tính
            </span>
          </Link>
        </Nav.Link>
      </Nav>
    </div>
  );
}
