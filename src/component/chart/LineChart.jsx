
import './LineChart.css';
import axios from "axios";
import { url } from "../../config/API";

import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush,
  AreaChart, Area,
} from 'recharts';



export default class Example extends PureComponent {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/nskpgcrz/';

  constructor(props) {
    super(props);
    this.state = {
      "lineChart": null,
      "pieChart": null,
      "monthData": [],
      "lineChartData": [],
    };
  }

  componentDidMount() {
    axios.get(url + "/join/get-summary-join-this-year").then(res => {

      this.setState({ "lineChart": res.data });
      var listObjData = [];
      var monthData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

      for (let i = 0; i < res.data.length; i++) {
        var date = new Date(res.data[i]._id.month);
        monthData[date.getMonth()] = res.data[i].Total;
      }

      for (let i = 0; i < monthData.length; i++) {
        listObjData.push({ name: "Tháng " + (i + 1), "số lượng": monthData[i] });
      }
      this.setState({ lineChart: null });
      this.setState({ "lineChartData": listObjData });
    }).catch(err => {
      console.log(err);
    })
  }



  render() {
    return (
      <div>
        <div className="label">
          <h3>Số Lược Học Sinh Tham Gia Khóa Học Theo Tháng</h3>
        </div>
        <LineChart
          width={600}
          height={300}
          data={this.state.lineChartData}
          syncId="anyId"
          margin={{
            top: 5, right: 5, left: 0, bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Line type="monotone" dataKey="số lượng" stroke="#82ca9d" fill="#82ca9d" />
          <Brush />
        </LineChart>

      </div>
    );
  }
}
