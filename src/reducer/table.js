import {
  GET_ALL_TABLE_SUCCESS,
  GET_ALL_TABLE_FAILURE,
  GET_AN_TABLE_SUCCESS,
  GET_AN_TABLE_FAILURE,
} from "../config/ActionTypes";

const init = {
  data: {
    allTable: [],
    table: {},
  },
  error: {
    allTable: null,
    table: null,
  },
};

export default function tableReducer(state = init, action) {
  switch (action.type) {

    case GET_ALL_TABLE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          allTable: action.data,
        },
      };

    case GET_ALL_TABLE_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          allTable: action.error,
        },
      };

    case GET_AN_TABLE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          table: action.data,
        },
      };
    case GET_AN_TABLE_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          table: action.error,
        },
      };
    default:
      return state;
  }
}
