import {
    GET_ALL_ATTRIBUTE_SUCCESS,
  } from "../config/ActionTypes";
  
  const init = {
    data: {
        allAttribute: [],
        tableType: {},
    },
    error: {
        allAttribute: null,
        tableType: null,
    },  
  };
  
  export default function tableTypeReducer(state = init, action) {
    switch (action.type) {
  
      case GET_ALL_ATTRIBUTE_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            allAttribute: action.data,
          },
        };


      default:
        return state;
    }
  }
  