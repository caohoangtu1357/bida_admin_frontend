import { combineReducers } from "redux";
import user from "./user";
import authenticate from "./authenticate";
import table from "./table";
import alert from "./alert";
import tableType from "./tableType";
import product from "./product";
import attribute from "./attribute";
import productType from "./productType";
import order from "./order";

const rootReducer = combineReducers({
  authenticate,
  user,
  table,
  alert,
  tableType,
  product,
  attribute,
  productType,
  order
});

export default rootReducer;
