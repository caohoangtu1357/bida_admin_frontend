import {
  GET_ALL_USER_SUCCESS,
  GET_ALL_USER_FAILURE,
  GET_AN_USER_SUCCESS,
  GET_AN_USER_FAILURE,
  DELETE_USER_FAILURE,
  DELETE_USER_SUCCESS,
} from "../config/ActionTypes";

const init = {
  data: {
    allUser: [],
    user: {},
  },
  error: {
    allUser: null,
    user: null,
  },
};

export default function userReducer(state = init, action) {
  switch (action.type) {
    case GET_ALL_USER_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          allUser: action.data,
        },
      };

    case GET_ALL_USER_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          allUser: action.error,
        },
      };

    case DELETE_USER_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
        },
      };

    case DELETE_USER_SUCCESS:
      return {
        ...state,
        error: {
          ...state.error,
        },
      };
    default:
      return state;
  }
}
