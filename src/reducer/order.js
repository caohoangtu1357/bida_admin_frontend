import {
    GET_ALL_ORDER_SUCCESS,
    GET_ALL_ORDER_FAILURE,
    GET_AN_ORDER_SUCCESS,
    GET_AN_ORDER_FAILURE,
  } from "../config/ActionTypes";
  
  const init = {
    data: {
      allOrder: [],
      product: {},
    },
    error: {
      allOrder: null,
      product: null,
    },
  };
  
  export default function productReducer(state = init, action) {
    switch (action.type) {
  
      case GET_ALL_ORDER_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            allOrder: action.data,
          },
        };
  
      case GET_ALL_ORDER_FAILURE:
        return {
          ...state,
          error: {
            ...state.error,
            allOrder: action.error,
          },
        };
  
      case GET_AN_ORDER_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            product: action.data,
          },
        };
      case GET_AN_ORDER_FAILURE:
        return {
          ...state,
          error: {
            ...state.error,
            product: action.error,
          },
        };
      default:
        return state;
    }
  }
  