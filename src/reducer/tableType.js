import {
    GET_ALL_TABLE_TYPE_SUCCESS,
    GET_AN_TABLE_TYPE_SUCCESS,
  } from "../config/ActionTypes";
  
  const init = {
    data: {
        allTableType: [],
        tableType: {},
    },
    error: {
        allTableType: null,
        tableType: null,
    },  
  };
  
  export default function tableTypeReducer(state = init, action) {
    switch (action.type) {
  
      case GET_ALL_TABLE_TYPE_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            allTableType: action.data,
          },
        };

      case GET_AN_TABLE_TYPE_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            tableType: action.data,
          },
        };

      default:
        return state;
    }
  }
  