import {
    GET_ALL_PRODUCT_SUCCESS,
    GET_ALL_PRODUCT_FAILURE,
    GET_AN_PRODUCT_SUCCESS,
    GET_AN_PRODUCT_FAILURE,
  } from "../config/ActionTypes";
  
  const init = {
    data: {
      allProduct: [],
      product: {},
    },
    error: {
      allProduct: null,
      product: null,
    },
  };
  
  export default function productReducer(state = init, action) {
    switch (action.type) {
  
      case GET_ALL_PRODUCT_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            allProduct: action.data,
          },
        };
  
      case GET_ALL_PRODUCT_FAILURE:
        return {
          ...state,
          error: {
            ...state.error,
            allProduct: action.error,
          },
        };
  
      case GET_AN_PRODUCT_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            product: action.data,
          },
        };
      case GET_AN_PRODUCT_FAILURE:
        return {
          ...state,
          error: {
            ...state.error,
            product: action.error,
          },
        };
      default:
        return state;
    }
  }
  