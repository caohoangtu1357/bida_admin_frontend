import {
    GET_ALL_PRODUCT_TYPE_SUCCESS,
  } from "../config/ActionTypes";
  
  const init = {
    data: {
        allProductType: [],
        tableType: {},
    },
    error: {
        allProductType: null,
        tableType: null,
    },  
  };
  
  export default function tableTypeReducer(state = init, action) {
    switch (action.type) {
  
      case GET_ALL_PRODUCT_TYPE_SUCCESS:
        return {
          ...state,
          data: {
            ...state.data,
            allProductType: action.data,
          },
        };


      default:
        return state;
    }
  }
  